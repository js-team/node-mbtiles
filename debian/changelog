node-mbtiles (0.3.2-3) unstable; urgency=medium

  * Team upload

  [ lintian-brush ]
  * Trim trailing whitespace.
  * Use secure copyright file specification URI.
  * Bump debhelper from deprecated 8 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Change priority extra to priority optional.
  * Set field Upstream-Name in debian/copyright.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update Vcs-* headers to use salsa repository.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + node-mbtiles: Drop versioned constraint on node-sphericalmercator and
      node-sqlite3 in Depends.

  [ Yadd ]
  * Update standards version to 4.6.0, no changes needed.
  * Fix filenamemangle
  * Fix GitHub tags regex
  * Drop dependency to nodejs
  * Use dh-sequence-nodejs auto install

 -- Yadd <yadd@debian.org>  Wed, 24 Nov 2021 18:34:14 +0100

node-mbtiles (0.3.2-2) unstable; urgency=low

  * Fix installation of schema.sql into /usr/share/node-mbtiles

 -- Jérémy Lal <kapouer@melix.org>  Wed, 11 Sep 2013 00:51:31 +0200

node-mbtiles (0.3.2-1) unstable; urgency=low

  * New upstream version
    + mbpipe, mbcheck, mbrekey, mbcompact executables are removed
  * Update install and patches to not install executables
  * control:
    + fix Homepage
    + loosen nodejs dependency
    + remove dependencies on node-step, node-underscore,
      node-optimist
    + canonicalize Vcs fields
    + rewrite short and long descriptions
  * copyright:
    + add Source field
    + use keyword BSD-3-clause

 -- Jérémy Lal <kapouer@melix.org>  Mon, 02 Sep 2013 02:02:39 +0200

node-mbtiles (0.2.3-2) unstable; urgency=low

  * Migrate from node to nodejs (Closes: #686900)
  * Standards-Version bump to 3.9.4, no changes needed

 -- David Paleino <dapal@debian.org>  Thu, 20 Sep 2012 12:18:14 +0200

node-mbtiles (0.2.3-1) unstable; urgency=low

  * New upstream version

 -- David Paleino <dapal@debian.org>  Sun, 08 Apr 2012 22:28:39 +0200

node-mbtiles (0.2.1-1) unstable; urgency=low

  * New usptream version
  * Bump runtime dependencies
  * Standards-Version bump to 3.9.3, no changes needed
  * Update debian/copyright

 -- David Paleino <dapal@debian.org>  Thu, 22 Mar 2012 22:22:04 +0100

node-mbtiles (0.1.17-1) unstable; urgency=low

  * New upstream version

 -- David Paleino <dapal@debian.org>  Wed, 16 Nov 2011 22:29:34 +0100

node-mbtiles (0.1.13-1) unstable; urgency=low

  * Initial release

 -- David Paleino <dapal@debian.org>  Wed, 26 Oct 2011 11:17:07 +0200
